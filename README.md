# Filepond with Livewire

[![php][php-version-src]][php-version-href]
[![laravel][laravel-version-src]][laravel-version-href]
[![livewire][livewire-version-src]][livewire-version-href]

This is a demo application to use [Filepond](https://pqina.nl/filepond/) with [Livewire](https://laravel-livewire.com/).

## Installation

You can install dependencies:

```bash
composer install
```

Install Node.js dependencies:

> You can install dependencies with `npm` or `yarn` but I recommend using `pnpm` because it's faster and more efficient.

```bash
pnpm install
```

Then you can copy the `.env.example` file to `.env` and generate the application key:

> You have to create database before running migrations.

```bash
cp .env.example .env
php artisan key:generate
```

Create symbolic link for storage:

```bash
php artisan storage:link
```

And finally you can run the migrations:

```bash
php artisan migrate:fresh --seed
```

## Usage

You can run the development server:

```bash
php artisan serve
```

And compile assets:

```bash
pnpm dev
```

And then access the application at [http://localhost:8000](http://localhost:8000)

[laravel-version-src]: https://img.shields.io/badge/dynamic/json?label=Laravel&query=require[%27laravel/framework%27]&url=https://gitlab.com/ewilan-riviere/filepond-livewire/-/raw/main/composer.json&color=777bb4&logo=laravel&logoColor=ffffff&labelColor=18181b
[laravel-version-href]: https://laravel.com/
[php-version-src]: https://img.shields.io/badge/dynamic/json?label=PHP&query=require[%27php%27]&url=https://gitlab.com/ewilan-riviere/filepond-livewire/-/raw/main/composer.json&color=777bb4&logo=&logoColor=ffffff&labelColor=18181b
[php-version-href]: https://www.php.net/
[livewire-version-src]: https://img.shields.io/badge/dynamic/json?label=Livewire&query=require[%27livewire/livewire%27]&url=https://gitlab.com/ewilan-riviere/filepond-livewire/-/raw/main/composer.json&color=777bb4&logoColor=ffffff&labelColor=18181b
[livewire-version-href]:https://livewire.laravel.com/
