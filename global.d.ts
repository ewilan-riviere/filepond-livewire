/// <reference types="vite/client" />

import type { Alpine as AlpineType } from 'alpinejs'

declare global {
  interface Window {
    Alpine: Alpine
    appUrlStorage: string
  }
}

window.Alpine = window.Alpine || {}
window.Alpine.version = ''

export {}
