<?php

namespace App\Livewire;

use App\Livewire\Traits\LiveNotify;
use App\Livewire\Traits\LiveUpload;
use App\Livewire\Traits\LiveValidator;
use App\Models\User;
use Livewire\Attributes\Rule;
use Livewire\Component;
use Livewire\Features\SupportFileUploads\TemporaryUploadedFile;
use Livewire\WithFileUploads;

class SettingsForm extends Component
{
    use LiveNotify;
    use LiveUpload;
    use LiveValidator;
    use WithFileUploads;

    public User $model;

    #[Rule('nullable|string|max:256')]
    public $name = '';

    /** @var null|string|TemporaryUploadedFile */
    #[Rule('nullable|image|max:2048')]
    public $avatar = '';

    /** @var null|string[]|TemporaryUploadedFile[] */
    #[Rule([
        'gallery' => 'nullable',
        'gallery.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ])]
    public $gallery = [];

    public function mount()
    {
        $this->model = User::query()->latest()->first();

        $this->name = $this->model->name;
        $this->avatar = $this->model->avatar;
        if ($this->isMultidimensional($this->model->gallery)) {
            $this->model->gallery = [];
            $this->model->update([
                'gallery' => $this->model->gallery,
            ]);
        }
        $this->gallery = $this->model->gallery;
    }

    public function save()
    {
        $images = [
            'avatar' => $this->avatar,
            'gallery' => $this->gallery,
        ];

        $this->uploadConfig($images);
        $this->validator();

        $this->model->update([
            'name' => $this->name,
        ]);

        $this->uploading($images);

        $this->notify()
            ->success();
    }

    public function render()
    {
        return view('livewire.settings-form');
    }
}
