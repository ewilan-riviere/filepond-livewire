<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
        'gallery',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'gallery' => 'array',
    ];

    protected $appends = [
        'avatar_url',
        'gallery_urls',
    ];

    public function getAvatarUrlAttribute(): string
    {
        return config('app.url') . "/storage/{$this->avatar}";
    }

    public function getGalleryUrlsAttribute(): array
    {
        return collect($this->gallery)->map(function ($image) {
            return config('app.url') . "/storage/{$image}";
        })->toArray();
    }
}
