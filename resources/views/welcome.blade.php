<x-app-layout>
  <main class="container mx-auto max-w-5xl px-3 lg:px-6">
    <livewire:settings-form />
  </main>
</x-app-layout>
