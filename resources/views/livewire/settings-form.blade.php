<div class="my-5">
  <div>
    <div>
      User {{ $model->name }}
    </div>
    @if ($model->avatar)
      <div>
        <img
          class="h-16 w-16 rounded-full object-cover"
          src="{{ $model->avatar_url }}"
          alt=""
        >
        <button wire:click="deleteImage('avatar')">
          Delete avatar
        </button>
      </div>
    @endif
    <div class="flex flex-wrap space-x-1">
      @foreach ($model->gallery_urls as $key => $gallery_url)
        <div>
          <img
            class="h-16 w-16 rounded-md object-cover"
            src="{{ $gallery_url }}"
            alt=""
          >
          <button wire:click="deleteImage('gallery', {{ $key }})">
            Delete {{ $key }}
          </button>
        </div>
      @endforeach
    </div>
  </div>
  <form
    class="mb-6 pb-10"
    wire:submit="save"
  >
    <input
      name="name"
      type="text"
      wire:model="name"
    >
    <x-field.upload
      name="avatar"
      label="Avatar"
      wire:model="avatar"
    />
    <x-field.upload
      name="gallery"
      label="Gallery"
      wire:model="gallery"
      multiple
      reorder
    />
    <div class="fixed bottom-0 left-0 mt-3 flex w-full bg-white py-3">
      <div class="mx-auto">
        <button
          class="rounded-md bg-indigo-600 px-3 py-1 text-white hover:bg-indigo-500"
          type="submit"
        >
          Save
        </button>
      </div>
    </div>
  </form>
</div>
