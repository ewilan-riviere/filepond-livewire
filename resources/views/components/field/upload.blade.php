@pushOnce('head')
  <link
    href="https://unpkg.com/filepond/dist/filepond.css"
    rel="stylesheet"
  >
  <link
    href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css"
    rel="stylesheet"
  >
  <style>
    .filepond--drop-label {
      background-color: white;
      border-width: 2px;
      border-style: dashed;
      border-color: lightgray;
      border-radius: 0.375rem;
      transition: background-color 0.1s ease;
    }

    .filepond--drop-label:hover {
      background-color: rgba(0, 0, 0, 0.01);
    }
  </style>
@endPushOnce

<div
  class="relative"
  wire:ignore
  x-data="{
      model: @entangle($attributes->whereStartsWith('wire:model')->first()),
      isMultiple: {{ $multiple ? 'true' : 'false' }},
      current: undefined,
      currentList: [],
      count: 0,
  
      async URLtoFile(path) {
          let url = `${window.appUrlStorage}/${path}`;
          let name = url.split('/').pop();
          const response = await fetch(url);
          const data = await response.blob();
          const metadata = {
              name: name,
              size: data.size,
              type: data.type
          };
          let file = new File([data], name, metadata);
          return {
              source: file,
              options: {
                  type: 'local',
                  metadata: {
                      name: name,
                      size: file.size,
                      type: file.type
                  }
              }
          }
      },
      notify(title = 'title', text = 'text', icon = 'heroicon-o-check-circle', color = 'success') {
          new FilamentNotification()
              .title(title)
              .body(text)
              .icon(icon)
              .iconColor(color)
              .seconds(2.5)
              .send()
      }
  }"
  x-cloak
  x-init="async () => {
      let picture = model
      let files = []
      let exists = []
      if (model) {
          if (isMultiple) {
              currentList = model.map((picture) => `${window.appUrlStorage}/${picture}`);
              let existsTemp = []
              await Promise.all(model.map(async (picture, key) => {
                  existsTemp.push({
                      src: await URLtoFile(picture),
                      id: key,
                  })
              }))
              existsTemp.sort((a, b) => a.id - b.id);
              exists = existsTemp.map((item) => item.src)
          } else {
              if (picture) {
                  exists.push(await URLtoFile(picture))
              }
          }
      }
      files = exists
      count = exists.length
      let modelName = '{{ $attributes->whereStartsWith('wire:model')->first() }}'
  
      function toggleDeleteButtons() {
          const deleteBtn = 'filepond--action-remove-item'
          let nodes = document.getElementsByClassName(deleteBtn)
          for (const iterator of nodes) {
              if (iterator.classList.contains('hidden')) {
                  iterator.classList.remove('hidden')
              } else {
                  iterator.classList.add('hidden')
              }
          }
      }
  
      const pond = FilePond.create($refs.{{ $attributes->get('ref') ?? 'input' }});
      const uploads = []
  
      if ({{ $multiple }}) {
          $wire.set('multipleCurrent', model)
      }
  
      pond.setOptions({
          allowMultiple: {{ $multiple ? 'true' : 'false' }},
          server: {
              process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
                  if ({{ $multiple }}) {
                      uploads.push(file)
                      @this.uploadMultiple(modelName, uploads, load, error, progress)
                      // toggleDeleteButtons()
                      count++
                  } else {
                      @this.upload(modelName, file, load, error, progress)
                  }
              },
              revert: (filename, load) => {
                  @this.removeUpload(modelName, filename, load)
                  count--
              },
              remove: (filename, load) => {
                  @this.deleteUpload(modelName, filename.name)
                  notify(
                      'File deleted',
                      `Your file is now deleted! You don't have to save to valid this action.`,
                      'heroicon-o-trash',
                      'info'
                  )
                  load();
                  count--
              },
          },
          allowImagePreview: {{ $preview ? 'true' : 'false' }},
          imagePreviewMaxHeight: {{ $previewMax ? $previewMax : '256' }},
          allowFileTypeValidation: {{ $validate ? 'true' : 'false' }},
          acceptedFileTypes: {{ $accept ? $accept : 'null' }},
          allowFileSizeValidation: {{ $validate ? 'true' : 'false' }},
          maxFileSize: {!! $size ? "'" . $size . "'" : 'null' !!},
          maxFiles: {{ $number ? $number : 'null' }},
          required: {{ $required ? 'true' : 'false' }},
          disabled: {{ $disabled ? 'true' : 'false' }},
          allowReorder: {{ $reorder ? 'true' : 'false' }},
          onprocessfile() {
              notify(
                  'File uploaded',
                  `You have to save to valid this action.`,
                  'heroicon-o-information-circle',
                  'info'
              )
          }
      });
      pond.addFiles(files)
  
      pond.on('addfile', (error, file) => {
          if (error) {
              new Notification()
                  .title('File upload failed')
                  .danger()
                  .send()
              return;
          }
      });
      pond.on('reorderfiles', (files, origin, target) => {
          @this.reorderUpload(modelName, origin, target)
      });
  }"
>
  @if ($label)
    <div class="flex items-center justify-between">
      <label
        class="block text-sm font-medium leading-6 text-gray-900 dark:text-gray-100"
        for="{{ $name }}"
      >
        {{ $label }}
        @if ($required)
          <span
            class="text-red-500"
            title="Required"
          >*</span>
        @endif
      </label>
      <div class="text-xs text-gray-400">
        Size max: {{ $sizeHuman }}
      </div>
    </div>
  @endif
  <div class="flex items-center justify-between text-xs text-gray-400">
    <div>
      Formats: {{ $acceptHuman }}
    </div>
    <div>
      {{ $multiple ? 'Multiple' : 'Single' }}
      @if ($multiple)
        <span>({{ $number }} files max)</span> | Currently <span x-text="count"></span>
      @endif
    </div>
  </div>
  <div class="mt-5">
    <input
      type="file"
      x-ref="{{ $attributes->get('ref') ?? 'input' }}"
    />
  </div>
</div>

@pushOnce('scripts')
  <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
  <script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.js"></script>
  <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js"></script>
  <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
  <script>
    FilePond.registerPlugin(FilePondPluginFileValidateType);
    FilePond.registerPlugin(FilePondPluginFileValidateSize);
    FilePond.registerPlugin(FilePondPluginImagePreview);
  </script>
@endPushOnce
